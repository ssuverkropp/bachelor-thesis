\chapter{The \Lsh algorithm}\label{Algorithm}
In automata learning, the goal is to learn the behaviour of a hidden Mealy machine.
The \Lsh algorithm accomplishes this using an observation tree to store the information that has been collected about the hidden Mealy machine.
In addition, it uses apartness to keep track of the states in the observation tree that cannot be equivalent. 

Most of this chapter is based on Section 3 of~\cite{Lsh}. Some of the proofs are from the appendix in the preprint version~\cite{Lsh_full}.

\section{Framework}
We use the MAT model, which stands for Minimally Adequate Teacher. This model was first proposed by Angluin in her seminal paper~\cite{angluin}.
In this framework, a learner tries to learn the behaviour of a hidden automata with the help of a teacher. The teacher answers to two types of queries.
In the original version for DFA's, these were the membership query and equivalence query.

The membership query indicates whether a word is accepted by the hidden DFA. The equivalence query checks if a given DFA is equivalent to the hidden DFA. If it isn't, a word is returned that is accepted by one, but not the other.

The \Lst algorithm introduced by Angluin in the same paper accomplished this in a polynomial number of queries. It uses an observation table to store the information from the membership queries.


The semantics of a Mealy machine does not correspond to some language, but produces output words.
To adapt the framework to this, the membership query is replaced by an output query, which gives the output word given a certain input word.
Given a hidden Mealy machine $M$, the output query is formally defined as follows:

\query{OutputQuery}{$\sigma$}{
  Returns the corresponding output word $\lambda^M(q_0^M, \sigma)$.
}
The equivalence query checks if a given Mealy machine is equivalent to $M$. If it isn't, an input word is given for which $H$ and $M$ give different outputs.

\query{EquivalenceQuery}{H}{
  Returns \ans{yes} if $H \approx M$. Otherwise it returns \ans{no}, together with an input word $\sigma$ such that $\lambda(a_0^M,\sigma) \neq \lambda(q_0^H,\sigma)$.
}

In practice, there is no teacher. The behaviour that we want to learn is from a system that is treated as a black box. That means we can give inputs and receive outputs, but we cannot observe the inner workings of the system.
In this situation, the equivalence query can't be answered with complete certainty.
Instead, the equivalence query is approximated. One way to do this is using conformance testing~\cite{practice}.

In conformance testing, we try to verify whether a system meets its specifications. In this case the specifications are given in terms of the Mealy machine.
To do this, we generate a test suite.
Ideally, we want the system to pass the test suite if and only if it corresponds to the given Mealy machine.
This is impossible to do in general.
However, with an additional assumption, like a maximum size for the Mealy machine that the system corresponds to, it is possible.

\section{Overview}
The algorithm builds an observation tree $T$ recording the results from all output queries. Initially $T$ consists of only the initial state $q_0$ and an empty transition function. The observation tree is split into three parts.
\begin{itemize}
\item
    The \emph{basis} $S\subseteq Q$ consists of the states that must represent different states in the hidden Mealy machine. All states in the basis are pairwise apart. That is, for every pair of distinct states in the basis $q,q'\in S, q\neq q'$ we know that $q \apart q'$. Initially, the basis consists of only the initial state, $S = \{q_0\}$. 
\item
    The \emph{frontier} $F\subseteq Q$ consists of the states that are not in the basis, but can be reached in one transition from a state in the basis. That is, 
    \[ F = \{q\in Q\setminus S \mid \exists q'\in S, i\in I . \delta(q,i) = q\} .\]
    These are the candidates to be added to the basis $S$.
\item
    The remaining states $Q\setminus (S\cup F)$.
\end{itemize}

Because the states that are added to the basis are always chosen from the frontier, the basis has the shape of a tree. More formally, all states in the basis except for the initial state can be reached by a transition from a state in the basis. Conversely, if for a state $q\in Q$ there is a transition $\delta(q,i)\in S$, $q$ must be in the basis.

%Output queries are used to extend the observation tree until we can construct a complete Mealy machine $H$ from the basis, for which $T$ is an observation tree. Then we use the equivalence query to check whether $H$ is indeed the correct Mealy machine. If it isn't, the input word that is returned is used to update $T$, so that $H$ will not be generated again.

Whenever we use an output query, the observation tree will be expanded to include the results from the output query.

The algorithm consists of four rules, \ref{R1} through \ref{R4}, which applied non-deterministically. These rules can be found in Section \ref{rules}. The algorithm halts only when it gets a response of \ans{yes} to an equivalence query. The first rule adds states from the frontier to the basis. The second and third rule expand the observation tree with output queries. The fourth rule creates a hypothesis to use with the equivalence query. This is a complete Mealy machine that is created from the basis of the observation tree. If the answer is to the equivalence query is \ans{no}, or if the hypothesis contradicts the information already encoded in the observation tree, then output queries are used to prevent the same hypothesis from being generated again.

% \begin{enumerate*}[label={(R\arabic*)}]
%     \item Add a state in the frontier to the basis if it is apart from all basis states.
%         Add state in frontier to basis
%     \item If a basis state has no outgoing transition for some input, use an output query to add it; 
%         
%     \item If a state in the frontier is not apart from multiple basis states, use an output query to ensure it is apart from at least one.
%     \item If neither rule (R1) nor (R2) can be applied, create a hypothesis, check if it is consistent with the observation tree. If it is, use the equivalence query. If the result is \ans{yes}, the hypothesis is returned. If it isn't, the counterexample is processed 
% \end{enumerate*}


Below is an example to illustrate how the algorithm works. The details are given in Section \ref{sec_Lsh}.

\subsection{Example}
Suppose we are learning the Mealy machine in Figure \ref{ex1}.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto]
        \node (q0) [state, initial text = {}] {\cont{$q_0$}};
        \node (q1) [state, right=of q0] {\cont{$q_1$}};
        \node (q2) [state, below=of q1] {\cont{$q_2$}};
        \path [-stealth, thick]
            (q0) edge[loop below] node             {a/A} ()
            (q0) edge[bend left]  node[above]      {b/B} (q1)
            (q1) edge[bend left]  node[below]      {a/A} (q0)
            (q1) edge[bend left]  node[right]      {b/B} (q2)
            (q2) edge[bend left]  node[below left] {a/C} (q1)
            (q2) edge[loop right] node             {b/B} ()
        ;
    \end{tikzpicture}
    \caption{The hidden Mealy machine $M$}
    \label{ex1}
\end{figure}

Initially, our observation tree will consist of only the initial state $t_0$, without any transitions. To indicate that the initial state is in the basis, it is coloured red.

\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto, scale=0.8,
        basis/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
        frontier/.style={pattern=north west lines, pattern color=blue,  distance=20pt}]
        \node (t0) [state, basis, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
    \end{tikzpicture}
    \caption{$T_1$: The initial observation tree}
    \label{ex2}
\end{figure}

When there are no outgoing transitions from a state in the basis, we use output queries to get them. 
This is Rule \ref{R2}. The output queries a and b will return A and B respectively, allowing us to expand our observation tree to Figure \ref{ex3}. Blue is used to mark the states in the frontier.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto, scale=0.8,
        basis/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
        frontier/.style={pattern=north west lines, pattern color=blue,  distance=20pt}]
        \node (t0) [state, basis, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
        \node (t1) [state, frontier] at (2,2) {\cont{$t_1$}};
        \node (t2) [state, frontier] at (2,0) {\cont{$t_2$}};
        \path [-stealth, thick]
            (t0) edge node {a/A} (t1)
            (t0) edge node[below left] {b/B} (t2)
        ;
    \end{tikzpicture}
    \caption{$T_2$: The observation tree after one step}
    \label{ex3}
\end{figure}

All states in the basis have outgoing transitions, and the states in the frontier aren't apart from all the states in the basis, so now we can construct a hypothesis for an equivalence query. A hypothesis is a Mealy machine consisting of the states in the basis, that is partially consistent with the information we have. Constructing a hypothesis is Rule \ref{R4}. This will look like Figure \ref{ex4}.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto]
        \node (q0) [state, initial text = {}] {\cont{$h_0$}};
        \path [-stealth, thick]
            (q0) edge[loop below] node             {a/A} ()
            (q0) edge[loop right] node             {b/B} ()
        ;
    \end{tikzpicture}
    \caption{$H_1$: The hypothesis generated from $T_2$}
    \label{ex4}
\end{figure}

This hypothesis is completely consistent with the observation tree, so we will use an equivalence query. 
Since this hypothesis is not equivalent to the hidden Mealy machine, we get a counterexample, for example bba. 
We always use an output query on the counterexample, which extends the observation tree to Figure \ref{ex5}.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto, scale=0.8,
        basis/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
        frontier/.style={pattern=north west lines, pattern color=blue,  distance=20pt}]
        \node (t0) [state, basis, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
        \node (t1) [state, frontier] at (2,2) {\cont{$t_1$}};
        \node (t2) [state, frontier] at (2,0) {\cont{$t_2$}};
        \node (t3) [state] at (4,0) {\cont{$t_3$}};
        \node (t4) [state] at (6,0) {\cont{$t_4$}};
        \path [-stealth, thick]
            (t0) edge node {a/A} (t1)
            (t0) edge node[below left] {b/B} (t2)
            (t2) edge node[below] {b/B} (t3)
            (t3) edge node[below] {a/C} (t4)
        ;
    \end{tikzpicture}
    \caption{The observation tree after processing the equivalence query}
    \label{ex5}
\end{figure}

Now $t_0$ and $t_2$ are apart. This means the frontier state $t_2$ is apart from all states in the basis, so we can add it to the basis. This is Rule \ref{R1}. Afterwards, we can also add $t_3$ to the basis, since it is now in the frontier, and it is apart from both $t_0$ and $t_2$. The next step is to use output queries to obtain outgoing transitions form the basis again. This results in Figure \ref{ex6}.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto, scale=0.8,
        basis/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
        frontier/.style={pattern=north west lines, pattern color=blue,  distance=20pt}]
        \node (t0) [state, basis, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
        \node (t1) [state, frontier] at (2,2) {\cont{$t_1$}};
        \node (t2) [state, basis] at (2,0) {\cont{$t_2$}};
        \node (t3) [state, basis] at (4,0) {\cont{$t_3$}};
        \node (t4) [state, frontier] at (6,0) {\cont{$t_4$}};
        \node (t5) [state, frontier] at (4,2) {\cont{$t_5$}};
        \node (t6) [state, frontier] at (6,2) {\cont{$t_6$}};
        \path [-stealth, thick]
            (t0) edge node {a/A} (t1)
            (t0) edge node[below left] {b/B} (t2)
            (t2) edge node[below] {b/B} (t3)
            (t3) edge node[below] {a/C} (t4)
            (t2) edge node[left] {a/A} (t5)
            (t3) edge node[left] {b/B} (t6)
        ;
    \end{tikzpicture}
    \caption{The observation tree after one step}
    \label{ex6}
\end{figure}

At this point, we could create another hypothesis. When we create a hypothesis, every frontier state is mapped to one state in the basis from which it is not apart. The transition going to that frontier state will then be replaced by a transition going to that state in the basis. For example, if $t_1$ is mapped to $t_0$, the transition $\transition{t_0}{a}{A}{t_1}$ will be replaced by $\transition{t_0}{a}{A}{t_0}$ as we have seen in the first hypothesis. This time, there are multiple states in the basis, and the frontier states are not yet apart from any of them. We will use Lemma \ref{weak_cotrans} to extend the apartness relation until we can map every frontier state onto just one basis state. This is Rule \ref{R3}. For example, to find out whether a state is apart from $t_3$ or $t_0$ we can observe the output with input a. If the output is A, it is apart from $t_3$ and if the output is C it is apart from $t_0$. An output of B makes it apart from both. Doing this gives us the observation tree in Figure \ref{ex7}.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto, scale=0.8,
        basis/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
        frontier/.style={pattern=north west lines, pattern color=blue,  distance=20pt}]
        \node (t0) [state, basis, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
        \node (t1) [state, frontier] at (2,2) {\cont{$t_1$}};
        \node (t2) [state, basis] at (2,0) {\cont{$t_2$}};
        \node (t3) [state, basis] at (4,0) {\cont{$t_3$}};
        \node (t4) [state, frontier] at (6,0) {\cont{$t_4$}};
        \node (t5) [state, frontier] at (4,2) {\cont{$t_5$}};
        \node (t6) [state, frontier] at (6,2) {\cont{$t_6$}};
        \node (t1a) [state] at (4,4) {\cont{$t_7$}};
        \node (t4a) [state] at (8,0) {\cont{$t_8$}};
        \node (t5a) [state] at (6,4) {\cont{$t_9$}};
        \node (t6a) [state] at (8,2) {\cont{$t_{10}$}};
        \path [-stealth, thick]
            (t0) edge node             {a/A} (t1)
            (t0) edge node[below left] {b/B} (t2)
            (t2) edge node[below]      {b/B} (t3)
            (t3) edge node[below]      {a/C} (t4)
            (t2) edge node[left]       {a/A} (t5)
            (t3) edge node[left]       {b/B} (t6)
            (t1) edge node[left]       {a/A} (t1a)
            (t4) edge node             {a/A} (t4a)
            (t5) edge node[left]       {a/A} (t5a)
            (t6) edge node             {a/C} (t6a)
        ;
    \end{tikzpicture}
    \caption{The observation tree when after output queries to distinguish between $t_0$ and $t_3$}
    \label{ex7}
\end{figure}

Now $t_1\apart t_3$, $t_4\apart t_3$ and $t_5 \apart t_3$, while $t_6$ is apart from both $t_0$ and $t_2$. Using more output queries we can also find apartness between $t_1\apart t_2$ and $t_4\apart t_0$ and $t_5 \apart t_2$.
Then all states in the frontier are apart from all but one state in the basis. 
We can map $t_1$ and $t_5$ to $t_0$, $t_4$ to $t_2$, and $t_6$ to $t_3$.
When we use this to construct another hypothesis, the result is identical to the hidden Mealy machine. This hypothesis is accepted by the equivalence query.

\section{Hypothesis}\label{hypothesis}
In the fourth rule of \Lsh, we create a Mealy machine on which we can use the equivalence query using the observation tree. This is called a hypothesis.

We assume an observation tree $T$ with basis $S$ and frontier $F$. The apartness relation and $\access$ function always refer to the observation tree $T$.

A hypothesis is constructed by building a Mealy machine whose states are the basis of $T$.
Transitions from the basis to the frontier in $T$ need to be replaced by transitions to the basis in the hypothesis. To do this, we map the states in the frontier to states in the basis.
We require that transitions in a hypothesis give the same output as their corresponding transitions in $T$, but a hypothesis might give different outputs for longer input words. 

\begin{definition}
    A Mealy machine $H$ \emph{contains the basis} if $Q^H = S$, and for all $q\in S$, $\delta^H(q_0,\access(q)) = q$.
    A \emph{hypothesis} is a complete Mealy machine $H$ that contains the basis, such that if $\transition{q}{i}{o}{p}$ in $T$, then $\transition{q}{i}{o}{p'}$ in $H$ and $\neg p \apart  p'$ for some $p'\in S$.
    A hypothesis $H$ is \emph{consistent} if there exists a functional simulation from $T$ to $H$.
    An input word $\sigma$ is said to \emph{lead to conflict} if $\delta^H(q_0, \sigma) \apart  \delta^T(q_0, \sigma)$.
\end{definition}

The frontier $F$ consists of all states that can be reached from the basis in one transition. 

\begin{definition}
    A state $q\in F$ is called:
    \begin{itemize}
    \item \emph{isolated}, if for all $q'\in S$, $q\apart q'$;
    \item \emph{identified}, if $q\apart q'$ for all but one $q'\in S$.
    \end{itemize}
\end{definition}

With these definitions, we can say something about the existence and uniqueness of a hypothesis.

\begin{lemma}[\m{hypothesis_existence}, \m{hypothesis_unique}]\label{hyp_exist}
    If none of the states in the frontier are isolated, there exists a hypothesis. If the basis is complete and all states in the frontier are identified, the hypothesis is unique.
\end{lemma}
\begin{proof}
    Because a hypothesis must contain the basis, $Q^H$ and $q_0^H$ are fixed.
    
    The only requirement for the output function is that for any transition in the basis $\lambda^T(q,i)=o$, we must have $\lambda^H(q,i)=o$.  Such an output function always exists, and if the basis is complete, this defines a unique function $\lambda^H$.

    Consider a relation $R\subseteq (S\times I) \times S$ defined by
    \[ R := \{((q, i), p) \mid \delta^T(q,i)\undefined \lor \neg( \delta^T(q,i) \apart p)\} . \]

    For $H$ to contain the basis, the transition function must map each pair $(q,i)$ in the basis for which $\delta^T(q,i)$ is also in the basis, to $\delta^T(q,i)$.
    Since the basis is pairwise apart, this requirement is fulfilled exactly when $\neg ( \delta^H(q,i) \apart \delta^T(q,i) )$. 
    For $H$ to be a hypothesis, we must also have $\neg ( \delta^H(q,i) \apart \delta^T(q,i) )$ for all other transitions.
    Thus, a transition function $\delta^H$ makes $H$ a hypothesis if and only if it is some subset of $R$.

    For every pair $(q,i) \in S\times I$ there are three cases:
    \begin{enumerate}
        \item
            $\delta(q,i) \in S$:
            Because the basis is pairwise apart, this means that $(q,i)$ is only related to $\delta(q,i)$.
        \item
            $\delta(q,i) \in F$:
            The pair $(q,i)$ is related to all states $p\in S$ from which $\delta(q,i)$ is not apart.
            If $\delta(q,i)$ is not isolated, there is at least one such state, and if $\delta(q,i)$ is identified, there is exactly one such state.
        \item
            $\delta(q,i) \undefined$:
            In this case $(q,i)$ is related to all states in the basis.
    \end{enumerate}
    If all pairs $(q,i)$ are related to at least one state, some complete function $\delta^H\subseteq R$ exists.
    In the first and third case, the pair $(q,i)$ is always related to some $p\in S$.
    In the second case, the pair is related to some $p\in S$ if the frontier state $\delta^T(q,i)$ is not isolated.
    Thus, a hypothesis exists if none of the states in the frontier are isolated.

    If all pairs $(q,i)$ are related to exactly one state, $R$ forms a transition function for $\delta^H$. That means that the hypothesis is unique.
    In the first case, the pair $(q,i)$ is only related to $\delta^T(q,i)$.
    In the second case, the pair is related to exactly one state if $\delta^T(q,i)$ is identified.
    In the third case, the pair might be related to many states, but this case will not occur if the basis is complete.
    So, if all states in the frontier are identified and the basis is complete, the hypothesis is unique.
\end{proof}

This result informs us when output queries need to be used to gather more information, and when a unique hypothesis can be constructed to provide in an equivalence query.
While isolated states exist, no hypothesis can be constructed, so we need to add them to the basis. 
When multiple hypotheses can be constructed, output queries are used to extend the observation tree until only one is left. This avoids using costly equivalence queries when output queries can be used as well.

\section{The \Lsh algorithm}\label{sec_Lsh}\label{rules}
The \Lsh algorithm consists of four rules which are applied nondeterministically.  The algorithm is shown in pseudocode in Algorithm \ref{Lsh}. The rules are as follows:
\begin{enumerate}[label=(R\arabic*)]
\item\label{R1} If a state in the frontier is isolated, that is to say, apart from all states in the basis, it must represent a different state in $M$, so it is added to the basis.
\item\label{R2} If for a certain state $q$ in the basis there is no outgoing transition for some input $i$, we can use an output query with $\access(q) i$ to define this transition. This then also extends the frontier.
\item\label{R3} If a state $q$ in the frontier is not apart from two different states $r$ and $r'$ in the basis, we can use rule (R3).
If $\sigma \vdash r \apart  r'$, then we use an output query with $\access(q) \sigma$. Lemma \ref{weak_cotrans} ensures that $q$ is now apart from at least one of $r$ and $r'$.
\item\label{R4} If none of the frontier states are isolated, and all outgoing transitions from the basis are defined, we can create a hypothesis. Then we check that it is consistent with the information in the observation tree. If it isn't, we use output queries to ensure that this hypothesis won't be generated in the future. If it is consistent, we use an equivalence query. 
\end{enumerate}
If neither of rules \ref{R1} and \ref{R2} are applicable, rule \ref{R4} must be applicable, so the algorithm never blocks. This also means that rule \ref{R3} is not strictly necessary. However, by using and prioritizing rule \ref{R3} above rule \ref{R4}, the algorithm becomes much faster.
\begin{algorithm}
\caption{The \Lsh algorithm}
\label{Lsh}
\begin{algorithmic}
\Procedure{LSharp}{}
    \DoIf{$q$ isolated for some $q\in F$}
        \Comment{Rule \ref{R1}}
        \State $S \gets S \cup \{q\}$
    \ElsDoIf{$\delta^T(q,i) \undefined$ for some $q\in S, i\in I$}
        \Comment{Rule \ref{R2}}
        \State $\outquery(\access(q)i)$
    \ElsDoIf{$\neg(q\apart r) \land \neg(q\apart r')$, for some $q\in F, r,r' \in S, r\neq r'$}
        \Comment{Rule \ref{R3}}
        \State $\sigma \gets \text{witness of }r \apart  r'$
        \State $\outquery(\access(q)\sigma)$
    \ElsDoIf{$F$ has no isolated states, and the basis $S$ is complete}
        \Comment{Rule \ref{R4}}
        \State $H \gets \textsc{BuildHypothesis}$
        \State $(b,\sigma) \gets \textsc{CheckConsistency}(H)$
        \If{$b=\ans{yes}$}
            \State $(b,\rho) \gets \equivquery(H)$
            \StateIf{$b = \ans{yes}$} \Return $H$
            \StateElse $\sigma \gets \text{shortest prefix of $\rho$ such that } \delta^H(q_0,\sigma) \apart  \delta^T(q_0,\sigma)$
        \EndIf
        \State $\textsc{ProcCounter}(H,\sigma)$
    \EndDoIf
\EndProcedure
\end{algorithmic}
\end{algorithm}


Algorithm \ref{Lsh} shows the \Lsh algorithm in pseudocode. 
It is important to note that whenever \textsc{OutputQuery} is used, the result will be used to update the observation tree $T$.

In the fourth rule of the algorithm, three subroutines are used: \textsc{BuildHypothesis}, \textsc{CheckConsistency} and \textsc{ProcCounter}. \textsc{BuildHypothesis} simply picks one of the possible hypotheses that are proven to exist by Lemma \ref{hyp_exist}. \textsc{CheckConsistency} checks whether the given hypothesis is \emph{consistent}. If it isn't, an input word is returned, for which the hypothesis and observation tree give different outputs. \textsc{ProcCounter} uses a number of output queries to ensure that the given hypothesis will not be generated again. 
The details of these subroutines can be found in the original paper about \Lsh~\cite{Lsh}.

Since the algorithm only halts if the equivalence query returns \ans{yes}, the correctness of the algorithm is proven by showing it terminates. This in turn can be done by showing that each of the rules expands $S$, $F$ or $\apart$ restricted to $S\times F$. These are all bounded by the size of the hidden Mealy machine. Again, the details of this proof can be found in the original paper about \Lsh~\cite{Lsh}.
