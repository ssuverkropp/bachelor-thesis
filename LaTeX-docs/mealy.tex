\chapter{Mealy machines and apartness}\label{mealy}
A Mealy machine is a type of finite-state machine that produces outputs when given inputs. As such, it represents a function from input words to output words. 
The \Lsh algorithm learns this function for some hidden Mealy machine.
It uses a partial Mealy machine to store the information that has been gathered about the hidden Mealy machine thus far.
In this chapter, we define partial Mealy machines and their semantics, as well as functional simulations which relate them and an apartness relation on states in Mealy machines.
This chapter is based on Section 2 of~\cite{Lsh}. Some of the proofs are taken from the appendix in the preprint version~\cite{Lsh_full}.
The names the lemmas refer to the names of the corresponding lemma in the Coq formalization.

\section{Partial functions and Kleene star}
First, we need some definitions and notations around partial functions.
A partial function $f\colon X\pto Y$ is defined as a set $f \subseteq X \times Y$, where for every $x\in X$, there is at most one $y\in Y$ such that $(x,y)\in f$. This forms a function from some subset of $X$ to $Y$.

If $(x,y)\in f$, we say that $f$ is defined on $x$, and write $f(x)\defined$ and $f(x)=y$. If for all $y\in Y$, $(x,y)\notin f$, $f$ is undefined on $x$, and we write $f(x)\undefined$.

Partial functions from $X$ to $Y$ are partially ordered by $\sqsubseteq$. For $f,g:X\pto Y$, we say that $f\sqsubseteq g$ if $f\subseteq g$ as sets. Equivalently, for all $x\in X$,
\[ f(x)\defined \implies g(x) \defined \land f(x) = g(x). \]
The composition of partial functions $f:X\pto Y$ and $g:Y\pto Z$, is $g \circ f : X \pto Z$ defined as follows. $g \circ f(x)\defined$ if $f(x) \defined$ and $g(f(x)) \defined$. Then we have $g \circ f(x) = g(f(x))$.

Given an alphabet $\Sigma$, we define $\Sigma^n$ to be the set of all words of length $n$ over that alphabet:
\[ \Sigma^0 = \{\empwor\} \]
\[ \Sigma^{i+1} = \{\letcon{a}{\sigma} \mid a \in \Sigma \land \sigma \in \Sigma^i\} \]
Then $\Sigma^*$ is the set of all words of any length over $\Sigma$.
\[ \Sigma^* = \bigcup_{i\in\mathbb{N}} \Sigma^i \]
We write $\letcon{a}{\sigma}$ for the concatenation of a letter $a\in\Sigma$ and a word $\sigma\in \Sigma^*$, and $\con{\sigma_1}{\sigma_2}$ for the concatenation of two words $\sigma_1,\sigma_2\in \Sigma^*$.

\section{Mealy machines}

A Mealy machine~\cite{mealy} is a finite-state machine, similar to a deterministic finite automaton (DFA). Where a DFA accepts certain input words based on accepting and non-accepting states, a Mealy machine produces an output. For each input letter, one output letter is generated, which is determined by both the state and input. For the input and output, we fix finite alphabets $I$ and $O$.
\begin{definition}\label{mealy_def}
    A partial Mealy machine is a quadruple $(Q,q_0, \delta, \lambda)$, where
    \begin{itemize}
    \item $Q$ is a finite set representing states
    \item $q_0\in Q$ is the initial state
    \item $\delta:Q\times I \pto Q$ is a partial function giving the next state
    \item $\lambda:Q\times I \pto O$ is a partial function giving the output
    \end{itemize}
    We require that $\delta$ and $\lambda$ are defined on the same pairs, so $\delta(q,i)\defined \iff \lambda(q,i)\defined$.
\end{definition}
We use a superscript to differentiate between the components of different Mealy machines, so $Q^M$ refers to the set of states of the Mealy machine $M$.
\begin{notation} 
    We write $\transition{q}{i}{o}{q'}$ for $\delta(q,i) = q'$ and $\lambda (q,i) = o$.
\end{notation} 
A Mealy machine $M$ is considered complete when $\delta^M$ and $\lambda^M$ are total functions.

We can extend the transition functions $\delta$ and $\lambda$ to apply them to input words instead of input letters. For $\delta$ on words, we compose the $\delta$ function with itself once for each input letter. For $\lambda$ on words, we concatenate the output letters from the transitions for each input letter.
The semantics of states in a Mealy machine is defined by how it transforms input words into output words.
\begin{definition}\label{semantics}
    We define the function $\delta^*:Q\times I^* \pto Q$ inductively by
    \[ \delta^*(q,\empwor) = q. \]
    \[ \delta^*(q,\letcon{a}{\sigma}) = \delta^*(\delta(q,a), \sigma). \]
    We define the function $\lambda^* Q\times I^* \pto O^*$ inductively by
    \[ \lambda^*(q,\empwor) = \empwor \]
    \[ \lambda^*(q,\letcon{a}{\sigma}) = \letcon{\lambda(q,a)}{\lambda^*(\delta(q,a), \sigma)}. \]
    Note that both of these definitions use the composition of partial functions, so $\lambda^*$ and $\delta^*$ are undefined if one of the transitions is undefined.
    From now on, we will omit the star, and also use $\delta$ and $\lambda$ on words.

    The semantics $\sem{q}$ of a state $q$ is defined as a partial function from words in $I$ to words in $O$ by
    \[ \sem{q}(\sigma) = \lambda(q,\sigma). \]
\end{definition}

Figure \ref{mealy_example} shows an example of a Mealy machine with states $Q = \{q_0, q_1, q_2, q_3, q_4\}$. This Mealy machine is not complete because there are no outgoing transitions from $q_4$, and because there is no outgoing transition from $q_3$ with input $a$.

For the initial state $q_0$, the semantics over input word $aba$ is as follows:
\[
    \sem{q_0}(aba)
    = \lambda(q_0,aba)
    = \con{B}{\lambda(q_1,ba)}
    = \con{BB}{\lambda(q_1,a)}
    = BBA
\]

On the other hand, for $q_1$, the semantics over $aba$ is not defined, since the third letter would be 
\[
    \lambda(\delta(q_1, ab), a)
    = \lambda(q_3, a)
    \undefined.
\]

\begin{figure}
\centering
\begin{tikzpicture}[node distance = 3cm, on grid, auto]
    \node (q0) [state, initial, initial text = {}] {$q_0$};
    \node (q1) [state, right=of q0] {$q_1$};
    \node (q2) [state, right=of q1] {$q_2$};
    \node (q3) [state, below=of q1] {$q_3$};
    \node (q4) [state, below=of q2] {$q_4$};
    \path [-stealth, thick]
    (q0) edge node[above] {a/B} (q1)
    (q0) edge node {b/B} (q3)
    (q1) edge node [below] {a/A} (q2)
    (q1) edge [loop above]  node {b/B}()
    (q2) edge node {b/B} (q3)
    (q3) edge node {b/A} (q4)
    (q2) edge [bend right] node [above] {a/B} (q1)
    ;
\end{tikzpicture}
\caption{An example of a Mealy machine over the sets $I=\{a,b\}$ and $O=\{A,B\}$}
\label{mealy_example}
\end{figure}

We can define equivalence of states and Mealy machines if their semantics are identical.
\begin{definition}\label{equivalence}
    Two states $q$ and $q'$ of a Mealy machine are equivalent if $\sem{q}=\sem{q'}$. Two Mealy machines $M$ and $N$ are equivalent if their initial states are equivalent. We write this as $q \approx q'$ for states and $M\approx N$ for Mealy machines.
\end{definition}

In the example from Figure \ref{mealy_example}, we can see that $q_0 \approx q_2$.

\section{Trees and functional simulations}
In the \Lsh algorithm, a Mealy machine is created that resembles a part of a hidden Mealy machine. We can formalize this notion using functional simulation.
This is a function that maps the states of one Mealy machine to the states of another, while preserving transitions between states.
\begin{definition}
    Let $M$ and $N$ be two Mealy machines. A \emph{functional simulation} $f:M \to N$ is a function $f\colon Q^M \to Q^N$ with
    \begin{itemize}
    \item $f(q_0^M) = q_0^N$, and
    \item if $\transition{q_1}{i}{o}{q_2}$ in $M$, then $\transition{f(q_1)}{i}{o}{f(q_2)}$ in $N$.
    \end{itemize}
\end{definition}

With this definition of a functional simulation, we can now relate this to the semantics of states in the Mealy machines.

\begin{lemma}[\m{func_sim_sem}]\label{funcsimsem}
    If $f:Q^M \to Q^N$ is a functional simulation, then for all $q\in Q^M$, 
    \[ \sem{q} \sqsubseteq \sem{f(q)} \]
\end{lemma}
\begin{proof}
    For all words $\sigma\in I^*$, if $\sem{q}(\sigma)\defined$, we need to show that $\sem{f(q)}(\sigma)\defined$, and that $\sem{q}(\sigma) = \sem{f(q)}(\sigma)$.
    This can be proven by structural induction on the word. For the empty word, the semantics of a state are defined to be $\empwor$ regardless of the state or Mealy machine. This makes the statement trivially true.\\
    For a non-empty word, say $\sigma = i:\tau$, we know that there must be a transition $\transition{q}{i}{o}{q'}$.
    The semantics is defined to be 
    \[ \sem{q}(\letcon{i}{\tau}) = \letcon{o}{\sem{q'}(\tau)} .\]
    Because $f$ is a functional simulation, that means $\transition{f(q)}{i}{o}{f(q')}$.
    Now we can write
    \[ \sem{f(q)}(\letcon{i}{\tau}) = \letcon{o}{\sem{f(q')}(\tau)} .\]
    With the induction hypothesis stating that $\sem{q}(\tau) = \sem{f(q')}(\tau)$, we can conclude that $\sem{q} \sqsubseteq \sem{f(q)}$.
\end{proof}

\begin{definition}
    A Mealy machine $T$ is a \emph{tree} if, for all states $q$, there is exactly one input word $\access(q)$ such that $\delta(q_0^M, \access(q)) = q$. It is called an \emph{observation tree} of a Mealy machine $M$ if it is a tree and there exists a functional simulation $f:T \to M$.
\end{definition}

In the \Lsh algorithm, we will build an observation tree of the hidden Mealy machine to store the information which we have accumulated.

\section{Apartness}
In the \Lsh algorithm, we build a complete Mealy machine out of an observation tree to give to the teacher. In doing this, we unify some states. We can never know for sure which states in the observation tree correspond to the same state in the hidden Mealy machine. Instead, we use the notion of apartness to look at the states that do not correspond to the same state in the hidden Mealy machine.

\begin{definition}
    Two states $q_1$ and $q_2$ are considered \emph{apart} if there is some input word $\sigma$ such that $\sem{q_1}(\sigma)\defined$, $\sem{q_2}(\sigma)\defined$ and $\sem{q_1}(\sigma) \neq \sem{q_2}(\sigma)$. The word $\sigma$ is then called the \emph{witness}.
\end{definition}

\begin{notation}
    If two states $q$ and $q'$ are apart, we write $q\apart q'$. If the word $\sigma$ is the witness of this, we write $\sigma \vdash q \apart q'$.
\end{notation}

To illustrate this, we can apply it in Example~\ref{mealy_example}. 
The state $q_4$ is not apart from any state, since it has no outgoing transitions. The states $q_0$ and $q_1$ are apart: $ \sem{q_0}(a) = B $, while $\sem{q_1}(a) = A$. This makes $a$ a witness, so we can write $a \vdash q \apart q'$. With input $b$, both $q_0$ and $q_1$ give $B$ as output, but another transition with input $b$ reveals that they are in fact apart. 

Note that for states in a complete Mealy machine, $q \apart q'$ iff $q \not\approx q'$.

We can now relate apartness to functional simulation, by showing that a functional simulation can never map two states that are apart to two states that are equivalent.

\begin{lemma}[\m{apart_func_sim}]\label{apart_func_sim}
    Let $f:M\to N$ be a functional simulation, and $q,q' \in Q^M$. If $q \apart q'$, then
    \[ f(q) \not\approx f(q') .\]
\end{lemma}
\begin{proof}
    Let $\sigma$ be a witness of $q\apart q'$. Then we know that $\sem{q}(\sigma)\defined$, $\sem{q'}(\sigma)\defined$ and $\sem{q}(\sigma) \neq \sem{q'}(\sigma)$. By Lemma \ref{funcsimsem}, we know that $\sem{f(q)}(\sigma) = \sem{q}(\sigma)$ and $\sem{f(q')}(\sigma) = \sem{q'}(\sigma)$. Using this, we see that 
    \[
        \sem{f(q)}(\sigma)
        = \sem{q}(\sigma)
        \neq \sem{q'}(\sigma)
        = \sem{f(q')}(\sigma)
        . \qedhere
    \]
\end{proof}

When two state $r,r'$ are apart, then we can show that a third state $q$ must be apart from at least one of them if its semantics are defined on a witness for $r \apart r'$. This property is called \emph{weak cotransitivity}.

\begin{lemma}[\m{weak_cotrans}]\label{weak_cotrans}
    Let $r,r',q \in Q^M$, and $\sigma \in I^*$. If $\sigma \vdash r \apart r'$, and $\sem{q}(\sigma)\defined$, then $r \apart q$ or $r' \apart q$.
\end{lemma}
\begin{proof}
    Because $\sigma$ is a witness of $r\apart r'$, we know that $\sem{r}(\sigma)\defined$ and $\sem{r'}(\sigma)\defined$, and we assumed that $\sem{q}(\sigma)\defined$. If both $\sem{r}(\sigma) = \sem{q}(\sigma)$ and $\sem{r'}(\sigma) = \sem{q}(\sigma)$, then it follows that $\sem{r}(\sigma) = \sem{r'}(\sigma)$. This contradicts our assumption, so $r\apart q$ or $r'\apart q$.
\end{proof}

This lemma can be used to reveal apartness relations during learning. For example, in figure \ref{mealy_example}, we noted earlier that $bb \vdash q_0 \apart q_1$, but $q_4$ is not apart from any state. If we add transitions for the word $bb$ to $q_4$, $q_4$ will be apart from at least one of $q_0$ and $q_1$.
