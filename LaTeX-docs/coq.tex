\chapter{Implementation in Coq}\label{coq}
I formalized everything from Chapter \ref{mealy} and Section \ref{hypothesis} in Coq. In short, this consists of partial Mealy machines, the observation trees and the construction of the hypothesis. I also proved Lemma~\ref{hyp_exist}, which states the hypothesis exists and is unique under certain circumstances.
The entire formalization consists of 774 lines of Coq code. It can be found in Appendix \ref{appendix}.
For this, I used the \stdpp library~\cite{stdpp}. 

\section{Coq and \stdpp}
Coq is a formal proof assistant based on the Calculus of Inductive Constructions.
It allows one to state theorems and prove them using proof tactics.
These proof tactics manipulate the hypotheses and goals of the proof.

It can also be used to write algorithms as a functional programming language.
These algorithms must always be proven to terminate.
We can also prove other properties about these algorithms.
Program extraction can then be used to extract an algorithm with certain verified properties.

The \stdpp library is a Coq library that is created to serve as an extension of the standard library~\cite{stdpp}.
It provides additional data structures like finite maps, as well as lemmas about them.
It uses typeclasses for common properties like finiteness or countability and overloaded notations like monad notations.

\section{Mealy machines}
Mealy machines are formalized as a record consisting of a set of states, an initial state and a transition function, together with a number of type class instances.
The transition function is represented with a \m{gmap}. This is a type for finite maps from \stdpp. This means that the map is defined on only finitely many inputs.
\begin{coqcode}
Record mealy (input output : Type) `{Countable input} := Mealy {
  Q :> Set;
  eqDecisionQ : EqDecision Q;
  finiteQ : Finite Q;
  q0 : Q;
  transition : gmap (Q * input) (Q * output)
}.
\end{coqcode}
In order to define a \m{gmap} on \m{Q * input}, we need it to be countable.
This is why we need countability of the input and the states.
Since \m{gmap} limits the number of transitions to a finite number, we do not need to require that the set of state and set of inputs are finite. The finite transition function then makes sure that only a finite number of inputs and states are actually used. 
For the input we do this, and we will only later require that there are finitely many inputs.

Only requiring countability of the set of states instead of finiteness in this definition caused problems with conflicting type classes later.
The countability of the set of states would then be derived from the finiteness in some cases, while a separate definition would be used in other cases.
This is why finiteness of \m{Q} is required here.
To define this, the equality of states needs to be decidable, so an instance of \m{EqDecision Q} is needed as well.

% \section{Semantics}
From the transition function, functions \m{delta} and \m{lambda} can be derived. Their outputs are \m{option Q} and \m{option output} to represent the fact that they are partial functions. Similarly, we define the semantics as follows.
\begin{coqcode}
Fixpoint sem (q : Q M) (is : list input) : option (list output) :=
  match is with
  | [] => Some []
  | i :: is => '(q',o) ← transition !! (q,i); (o ::.) <$> sem q' is
  end.
\end{coqcode}
In this definition, monad notation is used to compose partial functions.

\section{Observation trees and apartness}
The definition for a functional simulation is a straightforward translation to Coq. With the definition of a tree, the $\access$ function is included in the definition.
\begin{coqcode}
Definition tree (T:mealy input output) (access:T->list input) : Prop :=
  ∀ (q:T),
    repeat_delta (q0 T) (access q) = Some q
    /\ ∀ is, repeat_delta (q0 T) is = Some q -> is=access q.
\end{coqcode}

The alternative was to have an existential quantifier in the definition. In this version, the definition would look like this

\begin{coqcode}
Definition tree (T:mealy input output) : Prop :=
  ∃ (access:T -> list input), ∀ (q:T),
    repeat_delta (q0 T) (access q) = Some q
    /\ ∀ is, repeat_delta (q0 T) is = Some q -> is=access q.
\end{coqcode}
However, with this version the access function cannot be used in definitions of types in \m{Set}. This makes the definition of \m{contains_basis} more complex.

To check whether the created definitions were correct, I formalized the example from Figure 1 in~\cite{Lsh}, used to illustrate trees and functional simulation. Figure \ref{tree_example} shows this example. In this example, the colours represent how the function $f$ maps states from the left Mealy machine to the states of the right Mealy machine.

\begin{figure}
\centering
\begin{tikzpicture}[node distance=2cm, on grid, auto,
scale=0.8,
type1/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
type2/.style={pattern=north west lines, pattern color=blue,  distance=20pt},
type3/.style={pattern=crosshatch,       pattern color=green, distance=20pt}]
    \node (t0) [state, type1, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
    \node (t1) [state, type1] at (2,2) {\cont{$t_1$}};
    \node (t2) [state, type2] at (2,0) {\cont{$t_2$}};
    \node (t3) [state, type3] at (4,0) {\cont{$t_3$}};
    \node (t4) [state, type2] at (6,0) {\cont{$t_4$}};
    \node (t5) [state, type1] at (4,2) {\cont{$t_5$}};
    \path [-stealth, thick]
        (t0) edge node {a/A} (t1)
        (t0) edge node[below left] {b/B} (t2)
        (t2) edge node[left] {a/A} (t5)
        (t2) edge node[below] {b/B} (t3)
        (t3) edge node[below] {a/C} (t4)
    ;
    
    \path[->] (5,2) edge node {$f$} (7.5,2);
    
    \node (q0) [state, type1, initial, initial text = {}] at (9,2) {\cont{$q_0$}};
    \node (q1) [state, type2, right=of q0] {\cont{$q_1$}};
    \node (q2) [state, type3, below=of q1] {\cont{$q_2$}};
    \path [-stealth, thick]
        (q0) edge[loop below] node             {a/A} ()
        (q0) edge[bend left]  node[above]      {b/B} (q1)
        (q1) edge[bend left]  node[below]      {a/A} (q0)
        (q1) edge[bend left]  node[right]      {b/B} (q2)
        (q2) edge[bend left]  node[below left] {a/C} (q1)
        (q2) edge[loop right] node             {b/B} ()
    ;
\end{tikzpicture}
\caption{The example of an observation tree from Figure 1 in~\cite{Lsh}}
\label{tree_example}
\end{figure}

I used new \m{Inductive} types for the input and output sets, and for the states of the Mealy machines. To prove that these types are countable or finite, I used bijections with \m{fin} types. \m{fin n} is a type with $n$ instances, for which proofs of countability and finiteness are provided in \stdpp. 
The proofs that this is an observation tree are straightforward, but slow in terms of processing time. On my machine, it took 12 seconds to process the proof that this is a tree. This is mainly due to the need to show that the provided access function gives the only input word leading to a specific state. To prove this, every input word that ends up in some state is checked for every state.

To define apartness in Coq, I first defined what a witness is, and then derived the definition for apartness from that.
\begin{coqcode}
Definition witness : list input -> M -> M -> Prop := λ is q1 q2,
  is_Some (sem q1 is) /\
  is_Some (sem q2 is) /\
  sem q1 is <> sem q2 is.

Definition state_apart : M -> M -> Prop := λ q p,
  ∃ is, witness is q p.
\end{coqcode}

Lemmas \ref{apart_func_sim} and \ref{weak_cotrans} and their proofs could be translated to Coq in a very straightforward manner.

\section{Hypothesis}
To make the definitions and prove the lemmas detailed in Section \ref{hypothesis}, we need to work in a context with an observation tree with a basis and frontier. To do this, I used a \m{Section}, with various context statements corresponding to the assumptions in Section \ref{hypothesis}.

\begin{coqcode}
Context `{Finite input, EqDecision output, Inhabited output}.
Context (T:mealy input output) `{tree T access_T}.
Context (basis:T -> bool) (basis_q0 : basis (q0 T)).
Context (basis_apart: ∀ q p, basis q -> basis p -> ¬ (q # p) -> q = p).
Context (basis_tree: ∀ q p i, delta T q i = Some p -> basis p -> basis q).
\end{coqcode}
On the input type, we require a typeclass instance of \m{Finite} so we can go through all inputs. 
For the output type, we require typeclass instances of \m{EqDecision} and \m{Inhabited}. The \m{EqDecision output} instance is needed to ensure that it is decidable whether a transition should be in the basis or not.
The \m{Inhabited output} instance is needed to construct a hypothesis when some basis states are missing outgoing transitions.

Next, we have some assumptions about the basis. These are that the initial state is part of the basis, that the basis is pairwise apart, and that the basis has the shape of a tree. Without the last assumption, a state might be part of the basis, even though it can only be reached through states that are not part of the basis. Then the hypothesis constructed in the proof of Lemma \ref{hyp_exist} would not necessarily contain the basis.

To define a hypothesis, we need a type with all the states in the basis of $T$. For this purpose we use a sigma type \m{HS}. Inhabitants of this type consist of a pair of a state $q$ and a proof that $q$ is in the basis. Note that \m{q↾e} is notation from \stdpp for \m{exist _ q e}.

\begin{coqcode}
Definition HS :=  { q:T | basis q}.
\end{coqcode}

Instead of defining a hypothesis in terms of Mealy machines, I defined it in terms of a transition function. This makes the definitions simpler, since the transition function is the only part of the hypothesis that is not predetermined by the observation tree in all cases.

\begin{coqcode}
Section hypothesis.
  Context (h:gmap (HS * input) (HS * output)).
  Definition Hy : mealy input output :=
    Mealy HS _ HS_fin ((q0 T) ↾ basis_q0) h.

  Definition contains_basis : Prop :=
    ∀ (q:T) (e: basis q),
    repeat_delta (q0 Hy) (access_T q) = Some (q ↾ e).

  Definition hypothesis :=
    contains_basis
    /\ complete Hy
    /\ ∀ (q p:T) (i:input) (o:output) (e:basis q),
        transition T !! (q,i) = Some (p,o) ->
        ∃ (p': Hy), transition Hy !! (q↾e:Hy,i) = Some (p',o) /\ ¬p#`p'.

  Definition consistent := hypothesis ∧ ∃ f : T->Hy, func_sim f.

  Definition leads_to_conflict `{contains_basis} : list input -> Prop :=
    λ is,
      ∃ q q1 e,
      repeat_delta (q0 Hy) is = Some (q ↾ e)
      /\ repeat_delta (q0 T) is = Some q1
      /\ q # q1.
End hypothesis.
\end{coqcode}


\subsection{Hypothesis construction}
Lemma \ref{hyp_exist} is translated into two parts in the Coq code: the lemmas \m{hypothesis_existence} and \m{hypothesis_unique}. Before the proof of these lemmas can even start, a construction of a hypothesis is needed. This construction largely follows the construction given in the proof of Lemma \ref{hyp_exist}. Because of the way a hypothesis is defined in the Coq code, only a transition function is needed here. 
This transition function in turn is based on a filtered list called \m{h_list} using the \m{list_to_map} function. 

\begin{coqcode}
Let transition_in_basis : ((HS * input) * HS) -> Prop :=
  λ '(q1↾_, i, q2↾_),
    delta T q1 i = Some q2 /\ basis q2.

Let new_transition_fits : ((HS * input) * HS) -> Prop :=
  λ '(q1↾_, i, q2↾_),
    match delta T q1 i with
    | None => True
    | Some q1' => ¬ (q1' # q2) ∧ frontier q1'
    end.

Definition find_output : HS -> input -> output
  := λ q i, match lambda T (`q) i with
  | Some o => o
  | None => inhabitant
  end.

Definition add_output:
  ((HS * input) * HS) -> ((HS * input) * (HS * output))
  := λ '(q,i,p), ((q, i), (p, find_output q i)).

Let h_list := add_output <$> (filter 
    (λ x, transition_in_basis x ∨ new_transition_fits x)
    (enum (HS * input * HS))).
\end{coqcode}

The \m{transition_in_basis} function is for transitions $\transition{q}{i}{o}{q'}$ where $q,a'\in S$. Turning this into a separate category makes the proofs simpler, since transitions within the basis are usually already a case that has to be handled separately.

The \m{new_transition_fits} function is very similar to the constraint in the proof of Lemma \ref{hyp_exist}. However, it excludes transitions within the basis that are already captured by \m{transition_in_basis} by requiring that $\delta(q,i)$ is in the frontier. This is done to avoid duplicates and simplifies later reasoning.

The \m{add_output} function takes a triple and adds a fitting output to it. In case there is no outgoing transition, the inhabitant provided by the \m{Inhabited} typeclass is used.

A great difficulty of this definition is that it implicitly requires that the functions \m{transition_in_basis} and \m{new_transition_fits} are decidable. For the former, this is relatively straightforward.
For the latter, however, we need to show that apartness is decidable. Because there are infinitely many input words that could function as a witness for apartness, this is not easily derived. Using the access function, we can find a maximum length for input words for which the semantics of a state can be defined in a tree. This length is defined below by \m{max_access_len}. The lemma \m{tree_max_len} prove that this is the maximum length for which the semantics of a state can be defined.

\begin{coqcode}
Definition max_access_len (T:mealy input output) :
  (T-> list input) -> nat :=
  λ access, list_max (map (length ∘ access) (enum T)).

Lemma rev_tree_max_len {T:mealy input output} (access:T -> list input)
                                              (tree0: tree T access) :
  ∀ (is:list input) (q:T),
  is_Some (sem q is) -> length is ≤ (max_access_len T access).
\end{coqcode}

Now we can enumerate all input words shorter than this length. Deciding whether two states are apart then amounts to deciding whether there is a witness in that list of input words. In Coq, that means destructing an expression as follows.
\begin{coqcode}
destruct 
    (existsb
        (λ is:list input, bool_decide (witness is x y))
        (lists_shorter_than (max_access_len T access_T))
        ) eqn:E.
\end{coqcode}
In this expression, \m{existsb} is a boolean expression indicating whether there exists an element in a list fulfilling a requirement.

\subsection{Hypothesis existence}
The first part of Lemma \ref{hyp_exist}, which states a hypothesis exists if no states in the frontier are isolated, is encoded in Coq as follows:
\begin{coqcode}
Theorem hypothesis_existence :
  (∀(q:T) (Fq:frontier q), ¬isolated q Fq) -> (∃ h, hypothesis h).
\end{coqcode}

To prove this, we need to show three things: that \m{h} contains the basis, that it is complete, and that given a transition $\transition{q}{i}{o}{p}$ in $T$, \m{h} has a transition from $(q,i)$ to some state that is not apart from $p$.

To show that \m{h} contains the basis, we first show that \m{h} preserves all transitions that are in the basis.
\begin{coqcode}
Lemma h_preserves_transition :
  ∀ (q q1: T) (e:basis q) (e1:basis q1) (i:input) (o:output),
  transition T !! (q, i) = Some (q1, o) ->
  h !! ((q↾e):HS, i) = Some ((q1 ↾ e1):HS, o).
\end{coqcode}
With this lemma, we can show that \m{h} contains the basis with structural induction on the input word $\access(q)$.
This uses induction on the end of the word, using \m{ref_ind}.

Next, we show that \m{h} is complete. For a given pair of a state and an input $q,i$, we have three different cases to consider: the case where $\delta^T(q,i)\undefined$, the case where $\delta^T(q,i)$ is in the basis, and the case where $\delta^T(q,i)$ is in the frontier.

For the last part, I have defined another lemma: \m{h_preserves_output}. This lemma states that if a transition exists in $T$, the output of the corresponding transition in \m{h} is the same.
\begin{coqcode}
Lemma h_preserves_output :
  (∀(q:T) (Fq:frontier q), ¬isolated q Fq) ->
  (∀q e i, is_Some (transition T !! (q,i)) ->
    lambda T q i = lambda (Hy h) (q↾e) i).
\end{coqcode}

With this lemma, we can show that \m{h} is a hypothesis by making a case distinction on whether the result of the transition is in the basis or in the frontier.

\subsection{Hypothesis uniqueness}
The second part of Lemma \ref{hyp_exist}, which states the hypothesis is unique if all frontier states are identified and the basis is complete, is encoded in Coq as follows:
\begin{coqcode}
Theorem hypothesis_unique (BC:basis_complete) (FI:frontier_identified):
  ∃ h, ∀ h0, hypothesis h0 -> h=h0.
\end{coqcode}
In order to prove this, we first show that \m{h_list} can be interpreted as a function. This means that for any two pairs $(x_1,y_1)$ and $(x_2,y_2)$ in \m{h_list}, $x_1=x_2$ implies $y_1=y_2$.

\begin{coqcode}
Lemma h_list_functional : basis_complete -> frontier_identified ->
    ∀ x y1 y2, (x, y1) ∈ h_list -> (x, y2) ∈ h_list -> y1 = y2.
\end{coqcode}

With the help of this lemma, we can show that every pair of an input and state only occur once as the first part of a pair in \m{h_list}.

\begin{coqcode}
Lemma NoDup_h_list :
  basis_complete -> frontier_identified -> NoDup h_list.*1.
\end{coqcode}

This allows us to use the lemma \m{elem_of_list_to_map}, which states that a transition is in the \m{gmap} \m{h} if it is in \m{h_list}.
In order to prove the lemma \m{hypothesis_unique}, we have to prove that the transitions of any hypothesis \m{h0} must be equal to that of \m{h}. Given an input $i$ and a state $q$, we distinguish two cases: $\delta^T(q,i)$ is in the basis, or $\delta^T(q,i)$ is in the frontier. In the first case, we use the lemma \m{h_preserves_transition} to show that the transition in \m{h} is identical to the transition in $T$, and then we show that the transition in \m{h0} must be identical to that too.
In the second case, we can use the lemma \m{elem_of_list_to_map} to show that \m{h} sends $(q,i)$ to the only state in the basis that is not apart from $\delta^T(q,i)$. The state that \m{h0} sends $(q,i)$ must be the same state, since all states in the frontier are identified.


