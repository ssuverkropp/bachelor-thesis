\chapter{Introduction}\label{introduction}
Active automata learning was introduced in 1987 by Dana Angluin~\cite{angluin}. 
In active automata learning, a learner learns a regular language $L$ with the help of a teacher. The learner can ask two types of queries to the teacher: ``Is this word part of $L$?'' and ``Is $L$ the language accepted by automata $H$?''

Active automata learning is used in formal methods to analyse and verify systems without complete specifications~\cite{app1, black_box}.
In these cases, active automata learning is used to construct a behavioural model of the system.
Then, the model can be checked for certain properties.

In the same paper, Angluin also introduced the \Lst algorithm~\cite{angluin}, which can learn regular languages in polynomial time. Various improvements have been made on this algorithm~\cite{L1,L2,L3,L4,L5}. Isenberner showed that these algorithms all fit into a single framework~\cite{foundations}.

Active automata learning can also be applied to many other types of automata. 
For instance, \Lst has been adapted to different types of automata, like non-deterministic automata~\cite{L_nfa}, mealy machines~\cite{L_mealy1, L_mealy2}, I/O automata~\cite{L_io} and visibly pushdown automata~\cite{foundations}. 

Many of these algorithms are similar to \Lst, but with various improvements.
The \Lsh algorithm~\cite{Lsh}, which learns Mealy machines, differs in that tries to establish apartness of observations, instead of equivalence of observations.
Apartness is a constructive form of inequality~\cite{apartness_bisim}.
In other words, it tries to determine which inputs must lead us to different states, instead of determining which inputs might lead to the same state.
As its data structure it only uses a partial Mealy machine called an observation tree.
This makes it simpler to implement than some other algorithms, while also achieving the worst-case time complexity.

In this thesis I work towards a formalization of \Lsh in the formal proof management system Coq~\cite{coq}. Coq allows us to state and prove theorems and to export verified programs. It checks all proofs are valid, so we can verify that all proofs are completely correct.

I have not formalized the algorithm itself, but I have formalized Mealy machines and the observation tree and apartness relation that the algorithm relies on. In addition, I have formalized the construction of a hypothesis.
This is a Mealy machine that is given to the teacher for an equivalence query.

In Chapter 2, I will give background information about Mealy machines, observation trees and the apartness relation.
Chapter 3 gives an overview of the \Lsh algorithm, and explains in detail how hypothesis construction works.
Chapter 4 discusses the formalization in Coq.

Appendix A contains a code listing of the formalization.
The full code can also be found at:
\gitlink
