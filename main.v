From stdpp Require Import fin_maps gmap finite fin option.

Record mealy (input output : Type) `{Countable input} := Mealy {
  Q :> Set;
  eqDecisionQ : EqDecision Q;
  finiteQ : Finite Q;
  q0 : Q;
  transition : gmap (Q * input) (Q * output)
}.

Arguments Q {_ _ _ _} _.
Arguments transition {_ _ _ _ _}.
Arguments q0 {_ _ _ _} _.
Arguments Mealy {_ _ _ _} _ _ _ _ _.

Section instances.
  Context `{Countable input} {output:Type} {M:mealy input output}.
  Global Instance eqDecQ : EqDecision M := eqDecisionQ input output M.
  Global Instance finQ : Finite M := finiteQ input output M.
End instances.

Definition complete {input output:Type} `{Countable input} (M : mealy input output) :=
  ∀ (q:Q M) (i:input), is_Some (transition !! (q,i)).

Section semantics.
  Context `{Countable input} {output:Type}.
  Context  {M : mealy input output}.

  Definition lambda : Q M -> input -> option output := λ q i,
    snd <$> (transition !! (q, i)).

  Definition delta : Q M -> input -> option (Q M) := λ q i,
    fst <$> (transition !! (q, i)).

  Fixpoint repeat_delta (q:Q M) (is:list input) : option (Q M) :=
  match is with
  | nil => Some q
  | i::is' => q' ← delta q i; repeat_delta q' is'
  end.

  Lemma repeat_delta_app_Some : ∀ is1 is2 q q', 
    repeat_delta q is1 = Some q' ->
    repeat_delta q' is2 = repeat_delta q (is1 ++ is2).
  Proof.
  induction is1.
  - intros. inversion H0. done.
  - intros is2 q q'. simpl. destruct (delta q a). 2:done. apply IHis1.
  Qed.

  Lemma repeat_delta_app_None : ∀ is1 is2 q, 
    repeat_delta q is1 = None -> repeat_delta q (is1 ++ is2) = None.
  Proof.
  induction is1.
  - done.
  - simpl. intros is2 q. destruct (delta q a). 2:done. apply IHis1.
  Qed.

  Fixpoint sem (q : Q M) (is : list input) : option (list output) :=
    match is with
    | [] => Some []
    | i :: is => '(q',o) ← transition !! (q,i); (o ::.) <$> sem q' is
    end.

  Lemma is_Some_repeat_delta_sem :
    ∀ is q, is_Some (repeat_delta q is) ↔ is_Some (sem q is).
  Proof.
  split; revert q; induction is; (try done); intros; simpl;
  [destruct H0 as (q1 & H0) | destruct H0 as (os & H0)]; simpl in H0;
  [unfold delta in H0 | unfold delta ]; destruct (transition !! (q, a));
  try done; simpl; destruct p; simpl in H0.
  - apply fmap_is_Some. apply IHis. done.
  - simpl. apply IHis. elim fmap_is_Some with (cons o) (sem q1 is).
    intros. apply H1. done.
  Qed.

  Lemma sem_length :
    ∀ is q os,
      sem q is = Some os -> length is = length os.
  Proof.
    induction is; intros.
    - inversion H0. tauto.
    - simpl in H0. destruct (transition !! (q,a)); [..|done].
      destruct p, os. simpl in H0. destruct (sem q1 is); done.
      simpl. rewrite IHis with q1 os; [tauto|].
      destruct (sem q1 is) eqn:S.
      all: simpl in H0; rewrite S in H0; inversion H0; tauto.
  Qed.
End semantics.

Section functional_simulation.
   Context `{Countable input} {output:Type}.

  Arguments mealy _ _ {_ _}.

  Definition state_equiv {M N:mealy input output} : Q M -> Q N -> Prop :=
    λ q1 q2, ∀ is, sem q1 is = sem q2 is.
  Definition mealy_equiv : relation (mealy input output) := λ M N,
    state_equiv (q0 M) (q0 N).

  Definition func_sim {M N:mealy input output} : (M -> N) -> Prop :=
    λ f,
    f (q0 M) = q0 N /\
    ∀ q1 q2 i o, transition !! (q1,i) = Some (q2, o) ->
      transition !! (f q1,i) = Some (f q2, o).

  Definition partial_function_inclusion {A B}:
    relation (A -> option B) := λ f g,
    ∀ a b, f a = Some b -> g a = Some b.

  (*lemma 2.4*)
  Lemma func_sim_sem : ∀ {M N:mealy input output} (f:M -> N),
    func_sim f -> ∀ q:M, partial_function_inclusion (sem q) (sem (f q)).
  Proof.
  unfold partial_function_inclusion. intros M N f FS q is. revert q.
  induction is; try done. intros q os S. simpl in *.
  destruct (transition !! (q, a)) eqn:T. 2: inversion S.
  destruct p. destruct FS as [FS1 FS2]. rewrite FS2 with q q1 a o.
  2:done. simpl in *. destruct (sem q1 is) eqn:D; inversion S.
  rewrite IHis with q1 l; done.
  Qed.

  (* Trees *)
  Definition tree (T:mealy input output) (access:T->list input) : Prop :=
    ∀ (q:T),
      repeat_delta (q0 T) (access q) = Some q
      /\ ∀ is, repeat_delta (q0 T) is = Some q -> is=access q.

  Definition observation_tree (T M:mealy input output) : Prop :=
    (∃ ac, tree T ac) /\ ∃f: T->M, func_sim f.

  Definition max_access_len (T:mealy input output) :
    (T-> list input) -> nat :=
    λ access, list_max (map (length ∘ access) (enum T)).

  Lemma tree_max_len {T:mealy input output} (access : T -> list input) `(tree T access) :
    ∀ (is:list input) (q:T),
    (max_access_len T access) < length is -> ¬ is_Some (sem q is).
  Proof.
  intros. intro. apply is_Some_repeat_delta_sem in H1.
  rewrite repeat_delta_app_Some with (access q) is (q0 T) q in H1;
  [ | apply tree0].
  destruct H1 as (q' & H1). destruct tree0 with q'. apply H3 in H1.
  apply Nat.lt_lt_add_l with 
    (list_max (map (length ∘ access) (enum T))) (length is) (length (access q)) in H0.
  rewrite <- app_length in H0. rewrite H1 in H0.
  apply list_max_lt with
    (map (length ∘ access) (enum T)) (length (access q')) in H0.
  revert H0. apply Exists_not_Forall. apply Exists_exists.
  exists ((length ∘ access) q'). split.
  - apply in_map. apply elem_of_list_In. apply elem_of_enum.
  - simpl. nia.
  - intro. apply map_eq_nil in H5. apply elem_of_nil with q.
    rewrite <- H5. apply elem_of_enum.
  Qed.

  Lemma rev_tree_max_len {T:mealy input output} (access:T -> list input) (tree0: tree T access) :
    ∀ (is:list input) (q:T),
    is_Some (sem q is) -> length is ≤ (max_access_len T access).
  Proof.
  intros. apply Nat.le_ngt. intro.
  elim tree_max_len with access is q; done.
  Qed.

End functional_simulation.
Notation " x ~= y " := (state_equiv x y) (at level 70, no associativity).

Module tree_example.
  Inductive inp := a|b.
  Inductive out := A|B|C.
  Inductive Tstates := t0|t1|t2|t3|t4|t5.
  Inductive Mstates := m0|m1|m2.
  Definition fin2inp : nat -> inp := λ x, match x with
    | 0 => a
    | _ => b
    end.
  Definition inp2fin : inp -> nat := λ x, match x with
    | a => 0
    | b => 1
    end.
  Local Instance EqDecInp: EqDecision inp. solve_decision. Qed.
  Local Instance CountableInp : Countable inp.
  Proof.
  apply inj_countable' with inp2fin fin2inp. intro. destruct x; done.
  Defined.
  Local Instance EqDecisionTstates : EqDecision Tstates.
  solve_decision. Qed.
  Local Instance EqDecisionMstates : EqDecision Mstates.
  solve_decision. Qed.
  Definition fin2Tstates : (fin 6) -> Tstates := λ n,
    match n return Tstates with
    | 0%fin => t0
    | 1%fin => t1
    | 2%fin => t2
    | 3%fin => t3
    | 4%fin => t4
    | _ => t5
    end.
  Definition fin2Mstates : fin 3 -> Mstates := λ n,
    match n return Mstates with
    | 0%fin => m0
    | 1%fin => m1
    | _ => m2
    end.
  Local Instance FiniteTstates : Finite Tstates.
  Proof.
  apply surjective_finite with fin2Tstates. unfold Surj. intro.
  destruct y;
  [exists 0%fin|
    exists 1%fin|
    exists 2%fin|
    exists 3%fin|
    exists 4%fin|
    exists 5%fin].
  all:done.
  Qed.
  Local Instance FiniteMstates : Finite Mstates.
  Proof.
  apply surjective_finite with fin2Mstates. unfold Surj. intro.
  destruct y; [exists 0%fin|exists 1%fin|exists 2%fin]; done.
  Qed.

  Definition T_tr : gmap (Tstates * inp) (Tstates * out) :=
     <[(t0, a):=(t1, A)]>
    (<[(t0, b):=(t2, B)]>
    (<[(t2, a):=(t5, A)]>
    (<[(t2, b):=(t3, B)]>
    (<[(t3, a):=(t4, C)]>
    ∅)))).
  Definition M_tr : gmap (Mstates * inp) (Mstates * out) := 
     <[(m0, a):=(m0, A)]>
    (<[(m0, b):=(m1, B)]>
    (<[(m1, a):=(m0, A)]>
    (<[(m1, b):=(m2, B)]>
    (<[(m2, a):=(m1, C)]>
    (<[(m2, b):=(m2, B)]>
    ∅))))).
  Definition T : mealy inp out := Mealy Tstates _ _ t0 T_tr.
  Definition M : mealy inp out := Mealy Mstates _ _ m0 M_tr.
  Definition f : T -> M := λ t,
  match t with
    | t0|t1|t5 => m0
    | t2|t4 => m1
    | t3 => m2
  end.
  Lemma f_funcsim : func_sim f.
  Proof.
  unfold func_sim. split. done. intros. simpl in *. unfold T_tr in H.
  unfold M_tr. destruct q1, i; simpl_map; inversion H; done.
  Qed.

  Definition access_T : T -> list inp := λ t,
  match t with
    | t0 => nil 
    | t1 => cons a nil
    | t2 => cons b nil
    | t3 => cons b (cons b nil)
    | t4 => cons b (cons b (cons a nil))
    | t5 => cons b (cons a nil)
  end.

  Lemma T_tree : tree T access_T.
  Proof.
  unfold tree. intros. split.
  destruct q; simpl; unfold delta; simpl; unfold T_tr;
    simplify_map_eq; tauto.
  destruct q; intros is H; simpl in H.
  all: repeat (destruct is; simpl in H; try (inversion H; tauto);
      destruct i; unfold delta in H; simpl in H; unfold T_tr in H;
      simpl_map; simpl in H).
  Qed.

  Lemma T_obs_tree : observation_tree T M.
  Proof.
  unfold observation_tree. split. exists access_T. apply T_tree.
  exists f. apply f_funcsim.
  Qed.

End tree_example.

Section apartness.
  Context `{Countable input} `{EqDecision output} {M:mealy input output}.

  Definition witness : list input -> M -> M -> Prop := λ is q1 q2,
      is_Some (sem q1 is) /\
      is_Some (sem q2 is) /\
      sem q1 is <> sem q2 is.

  Definition state_apart : M -> M -> Prop := λ q p,
    ∃ is, witness is q p.

  Global Instance witness_dec :
  ∀ (is:list input) (q1 q2:M), Decision (witness is q1 q2).
  Proof.
  solve_decision.
  Qed.

  Lemma apart_irreflexive : ∀q, ¬ (state_apart q q).
  Proof.
  intro. unfold state_apart. intro. destruct H0.
  destruct H0 as (_ & _ & H0). destruct H0. tauto.
  Qed.
End apartness.

Notation " p # q " := (state_apart p q) (at level 60, no associativity).

Section apartness_lemmas.
  Context `{Countable input, EqDecision output}.
  (* lemma 2.7 *)
  Lemma apart_func_sim :
    ∀ {T M:mealy input output} (f:T->M) (q p:T),
    func_sim f -> q # p -> ¬ f q ~= f p.
  Proof.
  intros T M f q p FS A. unfold state_apart in A.
  destruct A as (is & ((out1 & S1) & (out2 & S2) & N)).
  unfold not, state_equiv. intro H0. apply N. rewrite S1. rewrite S2.
  rewrite <- func_sim_sem with f q is out1.
  rewrite <- func_sim_sem with f p is out2. all: done.
  Qed.

  (* lemma 2.8 *)
  Lemma weak_cotransitivity : 
    ∀ (M : mealy input output) (r r' q : M) (is : list input),
    (witness is r r') /\ (is_Some (sem q is)) -> (q # r) \/ (q # r').
  Proof.
  intros. destruct H0 as [W E]. destruct W as (H1 & H2 & N).
  inversion E. destruct H1 as (out1 & S1). destruct H2 as (out2 & S2).
  destruct (decide (x = out1)) as [->|]; [right | left];
  unfold state_apart; exists is; repeat split; try done.
  - rewrite H0. rewrite <- S1. tauto.
  - rewrite H0. rewrite S1. intro. inversion H1. done.
  Qed.
End apartness_lemmas.

Arguments transition {_ _ _ _} _.
Arguments lambda {_ _ _ _} _.
Arguments delta {_ _ _ _} _.

Section hypothesis_construction.
  Context `{Finite input, EqDecision output, Inhabited output}.
  Context (T:mealy input output) `{tree T access_T}.
  Context (basis:T -> bool) (basis_q0 : basis (q0 T)).
  Context (basis_apart: ∀ q p, basis q -> basis p -> ¬ (q # p) -> q = p).
  Context (basis_tree: ∀ q p i, delta T q i = Some p -> basis p -> basis q).
  Definition HS :=  { q:T | basis q}.
  Instance HS_fin : Finite HS.
  Proof.
  apply sig_finite. solve_decision. apply finiteQ.
  Qed.
  Lemma HS_eq : ∀(q q':T) (e:basis q) (e':basis q'),
    (q↾e:HS) = (q'↾e':HS) ↔ q = q'.
  Proof.
  intros. split. intros. inversion H1. done.
  intro. apply sig_eq_pi. intro. apply Is_true_pi. done.
  Qed.

  Section hypothesis.
    Context (h:gmap (HS * input) (HS * output)).
    Definition Hy : mealy input output :=
      Mealy HS _ HS_fin ((q0 T) ↾ basis_q0) h.

    Definition contains_basis : Prop :=
      ∀ (q:T) (e: basis q),
      repeat_delta (q0 Hy) (access_T q) = Some (q ↾ e).

    Definition hypothesis :=
      contains_basis
      /\ complete Hy
      /\ ∀ (q p:T) (i:input) (o:output) (e:basis q),
          transition T !! (q,i) = Some (p,o) ->
          ∃ (p': Hy), transition Hy !! (q↾e:Hy,i) = Some (p',o) /\ ¬p#`p'.

    Definition consistent := hypothesis /\ ∃ f : T->Hy, func_sim f.

    Definition leads_to_conflict `{contains_basis} : list input -> Prop :=
      λ is,
        ∃ q q1 e,
        repeat_delta (q0 Hy) is = Some (q ↾ e)
        /\ repeat_delta (q0 T) is = Some q1
        /\ q # q1.
  End hypothesis.

  Definition frontier (q:T) : Prop :=
    ¬ (basis q) /\
    exists q1 i, basis q1 /\ delta T q1 i = Some q.

  Global Instance frontier_dec : forall (q:T), Decision (frontier q).
  Proof.
  intro. solve_decision.
  Qed.

  Definition isolated (q:T) (frontier_q:frontier q) : Prop :=
    ∀ q1:T, ¬ basis q1 \/ q # q1.

  Definition identified (q:T) (frontier_q:frontier q) : Prop :=
    ∃ (q1:T) `(basis q1),
      ¬ (q # q1) /\ ∀ (q2:T) `(basis q2), (q # q2 \/ q2 = q1).

  Definition basis_complete : Prop :=
    ∀ (q:T) (i:input), basis q -> is_Some (transition T !! (q, i)).

  Let transition_in_basis : ((HS * input) * HS) -> Prop :=
    λ '(q1↾_, i, q2↾_),
      delta T q1 i = Some q2 /\ basis q2.

  Instance tib_decision : ∀p, Decision (transition_in_basis p).
  Proof.
  intros. repeat (destruct p). destruct h, h0. solve_decision.
  Qed.

  Global Instance fin_exists_dec `{Finite A} {P: A->Prop}:
    (∀ x:A, Decision (P x)) -> Decision (∃ x:A, P x).
  Proof.
  intro. destruct (decide (Exists P (enum A))).
  - left. apply Exists_exists in e. destruct e as (x & Inx & Px).
    exists x. tauto.
  - right. intro. destruct H2. destruct n. apply Exists_exists.
    exists x. split. 2:done. apply elem_of_list_In. apply elem_of_enum.
  Qed.

  Fixpoint lists_shorter_than `{Finite A} (n:nat) : list (list A) := 
    match n with
    | O => [[]]
    | S m => (lists_shorter_than m) ++
        (map proj1_sig (enum {l : list A | length l = n}))
    end.

  Lemma In_lists_shorter_than `{Finite A} (n:nat) : 
      ∀ l: list A, length l ≤ n -> In l (lists_shorter_than n).
  Proof.
  intros. induction n.
  - simpl. inversion H2. apply nil_length_inv in H4. left. done.
  - apply in_or_app. inversion H2.
    + right. apply in_map_iff. rewrite H4. exists (l ↾ H4). split. done.
      apply elem_of_list_In. apply elem_of_enum.
    + left. apply IHn. tauto.
  Qed.

  Lemma existsb_false_forall {A:Type} {P: A-> bool} {l: list A} :
      existsb P l = false -> ∀ x, In x l -> ¬ P x.
  Proof.
  intros. intro. induction l. inversion H2. simpl in H0.
  apply orb_false_elim in H1. destruct H1. inversion H2.
  - rewrite H5 in H1. rewrite H1 in H3. inversion H3.
  - apply IHl; done.
  Qed.

  Instance exists_witness_dec :
  ∀ x y: T, Decision (∃ is, witness is x y).
  Proof.
  intros.
  destruct 
      (existsb
          (λ is:list input, bool_decide (witness is x y))
          (lists_shorter_than (max_access_len T access_T))
          ) eqn:E.
  - left. apply Is_true_eq_left in E. apply existb_True in E.
    apply Exists_exists in E. destruct E as (is & I & W). exists is.
    apply bool_decide_unpack in W. apply W.
  - right. intro. destruct H1 as (is & W).
    elim tree_max_len with access_T is x.
    + done.
    + apply existsb_false_forall with is in E. exfalso. apply E.
      apply bool_decide_pack. apply W. apply In_lists_shorter_than.
      apply rev_tree_max_len with x. done. destruct W as (Sx1 & Sx & N).
      done.
    + destruct W. done.
  Qed.

  Let new_transition_fits : ((HS * input) * HS) -> Prop :=
    λ '(q1↾_, i, q2↾_),
      match delta T q1 i with
      | None => True
      | Some q1' => ¬ (q1' # q2) /\ frontier q1'
      end.

  Instance new_transition_fits_dec :
  ∀ x : HS * input * HS, Decision (new_transition_fits x).
  Proof.
  intros. unfold new_transition_fits.
  destruct x as (((q1&_)&i)&(q2&_)), (delta T (q1) i).
  all: solve_decision.
  Qed.

  Definition find_output : HS -> input -> output
    := λ q i, match lambda T (`q) i with
    | Some o => o
    | None => inhabitant
    end.

  Definition add_output:
    ((HS * input) * HS) -> ((HS * input) * (HS * output))
    := λ '(q,i,p), ((q, i), (p, find_output q i)).

  Lemma add_output_preserves : ∀ q1 i q2 o p,
    (q1, i, (q2, o)) = add_output p -> p = (q1,i,q2).
  Proof.
  intros q1 i q2 o p AO. unfold add_output in AO.
  destruct p as ((p11&p12)&p2). inversion AO. done.
  Qed.

  Let h_list := add_output <$> (filter 
      (λ x, transition_in_basis x \/ new_transition_fits x)
      (enum (HS * input * HS))).

  Let h: gmap (HS * input) (HS * output) := 
    list_to_map h_list.

  Lemma h_preserves_transition :
    ∀ (q q1: T) (e:basis q) (e1:basis q1) (i:input) (o:output),
    transition T !! (q, i) = Some (q1, o) ->
    h !! ((q↾e):HS, i) = Some ((q1 ↾ e1):HS, o).
  Proof.
  intros. unfold h. apply elem_of_list_to_map'.
  - intros. destruct x' as (q2 & o1). destruct q2 as (q2 & e2).
    apply elem_of_list_In in H2, H3. apply in_map_iff in H2,H3.
    destruct H2 as ((p1&q1')&F1&I1), H3 as ((p2&q2')&F2&I2).
    destruct p1 as (x1, i1), p2 as (x2, i2).
    unfold add_output in F1, F2. simpl in F1, F2. inversion F1.
    inversion F2. apply f_equal2. 2:done. apply HS_eq.
    rewrite H7, H8, H9 in I2. apply elem_of_list_In in I2.
    apply elem_of_list_filter in I2. destruct I2 as (D & _). destruct D.
    + unfold transition_in_basis in H2. destruct H2 as (D&_).
      unfold delta in D. rewrite H1 in D. inversion D. tauto.
    + unfold new_transition_fits in H2. unfold delta in H2. simpl in H2.
      rewrite H1 in H2. simpl in H2. destruct H2 as (_ & Fq1).
      destruct Fq1 as (Bq1 & _). elim Bq1. tauto.
  - apply elem_of_list_In. apply in_map_iff.
    exists ((q ↾ e, i), q1 ↾ e1). split.
    + unfold add_output, find_output. simpl. unfold lambda.
      rewrite H1. tauto.
    + apply elem_of_list_In. apply elem_of_list_filter.
      split. 2:apply elem_of_enum. left. unfold transition_in_basis. 
      split. 2:done. unfold delta. rewrite H1. tauto.
  Qed.

  Lemma list_to_map_prop `{EqDecision K} `{Countable K} {A:Type} :
    ∀ (P:A->Prop) (l:list (K*A)) (k:K),
    let M := list_to_map l:gmap K A in
    (∀ a, (k,a) ∈ l -> P a) -> is_Some (M !! k) ->
    ∃ a, M !! k = Some a /\ P a.
  Proof.
  intros. destruct H3 as (a & H3). exists a. split. done. apply H2.
  apply elem_of_list_to_map_2. unfold M in H3. tauto.
  Qed.

  Lemma h_contains_basis : contains_basis h.
  Proof.
  unfold contains_basis. intros. remember (access_T q) as is.
  revert Heqis e. revert q. induction is using rev_ind.
  - intros. simpl. apply f_equal. apply HS_eq. pose proof tree0 q.
    destruct H1. rewrite <- Heqis in H1. inversion H1. done.
  - intros. destruct (repeat_delta (q0 T) is) eqn:RDis.
    cut (Is_true (basis q1)). intro e1.
    rewrite <- repeat_delta_app_Some with is [x] (q0 (Hy h)) (q1 ↾ e1).
    + destruct tree0 with q. rewrite <- Heqis in H1.
      rewrite <- repeat_delta_app_Some with is [x] (q0 T) q1 in H1.
      2:done. simpl in H1. unfold delta in H1.
      destruct (transition T !! (q1, x)) eqn:tq1. 2:done.
      destruct p. simpl in H1. inversion H1. simpl in H4.
      rewrite H4 in tq1. simpl. unfold delta. simpl.
      rewrite h_preserves_transition with q1 q e1 e x o; done.
    + apply IHis. destruct tree0 with q1. apply H2. done.
    + destruct tree0 with q. apply basis_tree with q x. 2:done.
      rewrite <- Heqis in H1.
      rewrite <- repeat_delta_app_Some with is [x] (q0 T) q1 in H1.
      2:done. simpl in H1. destruct (delta T q1 x); tauto.
    + destruct tree0 with q. rewrite <- Heqis in H1.
      rewrite repeat_delta_app_None with is [x] (q0 T) in H1; done.
  Qed.

  Lemma h_complete :
    (∀(q:T) (Fq:frontier q), ¬isolated q Fq) -> complete (Hy h).
  Proof.
  unfold complete. intros. simpl. unfold is_Some.
  destruct (h !! (q,i)) eqn:H2. exists p; tauto. exfalso.
  apply not_elem_of_list_to_map in H2. apply H2.
  apply elem_of_list_fmap. destruct (transition T !! (`q, i)) eqn:Tqi.
  destruct p, (basis q1) eqn:e1.
  - apply Is_true_true_2 in e1. exists ((q, i), (q1 ↾ e1, o)).
    split. done. apply elem_of_list_In. apply in_map_iff.
    exists (q,i,q1 ↾ e1). split.
    { unfold add_output. simpl. repeat (apply f_equal2); try tauto.
      unfold find_output. unfold lambda. rewrite Tqi. tauto. }
    apply elem_of_list_In. apply elem_of_list_filter.
    split. 2:apply elem_of_enum. left. destruct q as (q&e).
    split. 2:tauto. unfold delta. simpl in Tqi. rewrite Tqi. tauto.
  - assert (frontier q1) as Fq1.
    { split. apply Is_true_false. tauto. exists (`q). exists i.
      split. destruct q. tauto. unfold delta. rewrite Tqi. tauto. }
    specialize H1 with q1 Fq1. unfold isolated in H1.
    assert (∃ q2, ¬ (¬ basis q2 \/ q1 # q2)).
    { apply Exists_finite.
      replace (λ x : T, ¬ (¬ basis x \/ q1 # x))
          with (not ∘ λ x, ¬ basis x \/ q1 # x).
      2:reflexivity. apply not_Forall_Exists. solve_decision. intro.
      apply H1. rewrite Forall_finite in H3. tauto. }
    destruct H3 as (q2 & H3). apply Decidable.not_or in H3.
    destruct H3 as (H3 & NA). destruct (basis q2) eqn:Bq2.
    2:naive_solver. apply Is_true_true in Bq2.
    exists (q, i, ((q2↾Bq2), o)). split. done. apply elem_of_list_fmap.
    exists (q,i,q2 ↾ Bq2). split.
    + unfold add_output. simpl. unfold find_output. unfold lambda.
      rewrite Tqi. tauto.
    + apply elem_of_list_filter. split. 2:apply elem_of_enum.
      right. unfold new_transition_fits. unfold delta. simpl.
      destruct q as (q&e). simpl in Tqi. rewrite Tqi. split; tauto.
  - exists ((q, i), (q, inhabitant)). split. done.
    apply elem_of_list_fmap. exists (q,i,q). split.
    + unfold add_output. simpl. unfold find_output. unfold lambda.
      rewrite Tqi. tauto.
    + apply elem_of_list_filter. split. 2:apply elem_of_enum.
      right. unfold new_transition_fits. unfold delta. simpl.
      destruct q as (q&e). simpl in Tqi. rewrite Tqi. done.
  Qed.

  Lemma h_preserves_output :
    (∀(q:T) (Fq:frontier q), ¬isolated q Fq) ->
    (∀q e i, is_Some (transition T !! (q,i)) ->
      lambda T q i = lambda (Hy h) (q↾e) i).
  Proof.
  intros. destruct H2 as (p&tq). destruct p. unfold lambda. rewrite tq.
  simpl. destruct (basis q1) eqn:e1.
  + apply Is_true_eq_left in e1.
    rewrite h_preserves_transition with q q1 e e1 i o; tauto.
  + assert (frontier q1).
    { split. apply Is_true_false. tauto. exists q, i.
      split; auto. unfold delta. rewrite tq. tauto. }
    unfold h. assert (H4:= H1 q1 H2). unfold isolated in H4. symmetry.
    apply fmap_Some. apply list_to_map_prop. 
    * intros. apply elem_of_list_fmap in H3.
      destruct H3 as ((p, q2) & A & _). destruct p as (qe, i2).
      unfold add_output in A. simpl in A. inversion A. simpl.
      unfold find_output, lambda. simplify_eq. simpl. rewrite tq. tauto.
    * apply h_complete. tauto.
  Qed.

  (* Lemma 3.6 part 1 *)
  Theorem hypothesis_existence :
    (∀(q:T) (Fq:frontier q), ¬isolated q Fq) -> (∃ h, hypothesis h).
  Proof.
  intros FI. exists h. repeat split.
  apply h_contains_basis. apply h_complete. tauto.

  intros q p i o e tqT. destruct (basis p) eqn:Bp.
  - apply Is_true_eq_left in Bp. exists (p↾Bp). split_and.
    2: apply apart_irreflexive. simpl.
    rewrite h_preserves_transition with q p e Bp i o. tauto. tauto.
  - assert (frontier p) as Fp.
    { split. apply Is_true_false. tauto. exists q,i.
      split. done. unfold delta. rewrite tqT. tauto. }
    destruct (h_complete FI (q↾e:HS) i). destruct x as (p' & o').
    exists p'. split.
    + assert (lambda (Hy h) (q↾e) i = Some o').
      { unfold lambda. rewrite H1. tauto. }
      rewrite <- h_preserves_output in H2. unfold lambda in H2.
      rewrite tqT in H2. simpl in H2. inversion H2. tauto. tauto.
      exists (p,o). tauto.
    + simpl in H1. unfold h in H1. apply elem_of_list_to_map_2 in H1.
      apply elem_of_list_fmap in H1. destruct H1 as (((q1&i1)&p1)&AO&IL).
      inversion AO. simplify_eq. simpl in *. simplify_eq.
      apply elem_of_list_filter in IL. destruct IL as (IL&_).
      destruct IL.
      * destruct p1, H1. simpl in H1. unfold delta in H1.
        rewrite tqT in H1. inversion H1. apply apart_irreflexive.
      * unfold new_transition_fits in H1. unfold delta in H1.
        simpl in H1. rewrite tqT in H1. simpl in H1.
        destruct p1 as (p1&e'), H1. tauto.
  Qed.

  Definition frontier_identified :=
    (∀(q:T) (Fq:frontier q), identified q Fq).

  Lemma h_list_functional : basis_complete -> frontier_identified ->
      ∀ x y1 y2, (x, y1) ∈ h_list -> (x, y2) ∈ h_list -> y1 = y2.
  Proof.
  intros BC FI x y1 y2 IL1 IL2. unfold h_list in IL1, IL2.
  destruct x as ((q&e) & i), y1 as (q1 & o1), y2 as (q2 & o2).
  apply elem_of_list_fmap in IL1,IL2.
  destruct IL1 as (y1&AO1&IL1), IL2 as (y2&AO2&IL2).
  pose proof (add_output_preserves (q↾e) i q1 o1 y1 AO1).
  pose proof (add_output_preserves (q↾e) i q2 o2 y2 AO2).
  simplify_eq. apply f_equal2.
  - unfold basis_complete in  BC. destruct (BC q i e) as ((q'&o) & Tqi).
    apply elem_of_list_filter in IL1,IL2.
    destruct IL1 as (IL1&_), IL2 as (IL2&_).
    destruct IL1, IL2;
    unfold transition_in_basis, new_transition_fits, delta in H1,H2.
    + destruct q1 as (q1&e1), q2 as (q2&e2).
      apply HS_eq. destruct H1,H2. rewrite H1 in H2. inversion H2. done.
    + exfalso. simpl in H2. rewrite Tqi in H1,H2. simpl in H1,H2.
      destruct q1 as (q1&e1), H1 as (H1&_). inversion H1. simplify_eq.
      destruct q2 as (q2&e2), H2 as (_&(H2&_&_)). done.
    + exfalso. simpl in H1. rewrite Tqi in H1,H2. simpl in H1,H2.
      destruct q2 as (q2&e2), H2 as (H2&_). inversion H2. simplify_eq.
      destruct q1 as (q1&e1), H1 as (_&(H1&_&_)). done.
    + simpl in H1,H2. rewrite Tqi in H1,H2.
      destruct q1 as (q1&e1), q2 as (q2,e2). simpl in H1,H2.
      destruct H1 as (NA1&F), H2 as (NA2&_). apply HS_eq.
      destruct FI with q' as (x&_&_&D). tauto.
      destruct (D q1 e1), (D q2 e2); try done. simplify_eq. tauto.
  - unfold add_output in AO1,AO2. inversion AO1. inversion AO2. done.
  Qed.

  Lemma NoDup_h_list :
    basis_complete -> frontier_identified -> NoDup h_list.*1.
  Proof.
  intros BC FI. apply NoDup_fmap_fst. apply h_list_functional; done.
  apply NoDup_fmap.
  - unfold Inj. intros p1 p2 AO.
    destruct p1 as ((x1&i1)&y1), p2 as ((x2&i2)&y2).
    unfold add_output in AO. simpl in AO. inversion AO. tauto.
  - apply list.NoDup_filter. apply NoDup_enum.
  Qed.

  (* Lemma 3.6 part 2 *)
  Theorem hypothesis_unique (BC:basis_complete) (FI:frontier_identified):
    ∃ h, ∀ h0, hypothesis h0 -> h=h0.
  Proof.
  exists h. intros h0 Hh0. apply map_eq. intro i.
  unfold hypothesis in Hh0. destruct Hh0 as (CB & C & O), i as (Hq & i).
  destruct Hq as (q & e), (BC q i e) as ((q1,o) & Tqi).
  destruct (basis q1) eqn:Bq1.
  - apply Is_true_true in Bq1.
    rewrite h_preserves_transition with q q1 e Bq1 i o. 2:done.
    unfold contains_basis in CB.
    assert (lambda (Hy h0) (q ↾ e) i = Some o).
    { destruct (O q q1 i o e Tqi). destruct H1. unfold lambda.
      rewrite H1. done. }
    assert (repeat_delta (q ↾ e:Q (Hy h0)) [i] = Some (q1↾Bq1:HS)).
    { rewrite repeat_delta_app_Some
        with (access_T q) [i] (q0 (Hy h0)) (q↾e).
      assert (access_T q ++ [i] = access_T q1). apply tree0.
      rewrite <- repeat_delta_app_Some with (access_T q) [i] (q0 T) q.
      unfold repeat_delta. unfold delta. rewrite Tqi. tauto.
      apply tree0. rewrite H2. apply CB. apply CB. }
    unfold repeat_delta in H2. destruct (delta (Hy h0) (q↾e) i) eqn:D;
    inversion H2. rewrite H4 in D. unfold delta in D.
    unfold lambda in H1.
    destruct (transition (Hy h0) !! (q ↾ e:(Q (Hy h0)), i)) eqn:DH.
    2:inversion D. simpl in DH. rewrite DH. simpl in H1,D. destruct p.
    inversion H1. inversion D. tauto.
  - assert (frontier q1) as Fq1.
    { split. apply Is_true_false. tauto. exists q. exists i.
      split. tauto. unfold delta. rewrite Tqi. tauto. }
    destruct (FI q1 Fq1) as (q2 & e2 & NA & I).
    assert (h !! (q ↾ e:HS, i) = Some (q2↾e2:HS, o)).
    { unfold h. apply elem_of_list_to_map. apply (NoDup_h_list BC FI).
      unfold h_list. apply elem_of_list_fmap.
      exists (q ↾ e, i, q2 ↾ e2). split.
      + unfold add_output. simpl. repeat apply f_equal2; try done.
        unfold find_output, lambda. simpl. rewrite Tqi. done.
      + apply elem_of_list_filter. split. 2: apply elem_of_enum.
        right. unfold new_transition_fits. simpl. unfold delta.
        rewrite Tqi. done.
    }
    rewrite H1. destruct (O q q1 i o e Tqi) as (q3 & Tr2 & NA2).
    simpl in Tr2. rewrite Tr2. apply f_equal. apply f_equal2. 2:tauto.
    destruct q3 as (q3 & e3). simpl in NA2. destruct (I q3 e3).
    + done.
    + apply HS_eq. done.
  Qed.
End hypothesis_construction.

Section teacher. 
  Context `{Finite input} (output : Type).

  Inductive equivAns :=
    | equiv : equivAns
    | nonEquiv : list input -> equivAns.

  Record teacher := Teacher {
    outputQuery : list input -> list output;
    equivQuery : ∀ (H:mealy input output) `(complete H), equivAns;
    teacherConsistent : ∃ (M:mealy input output),
      complete M
      /\ ∀ is : list input, Some (outputQuery is) = sem (q0 M) is
      /\ ∀ (H:mealy input output) (CH: complete H),
        match (equivQuery H CH) with
        | equiv => mealy_equiv M H
        | nonEquiv is => sem (q0 M) <> sem (q0 H)
        end
  }.
End teacher.
