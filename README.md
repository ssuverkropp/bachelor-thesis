# L# in Coq
This repository contains a Coq formalization of the preliminaries of the L# algorithm, as well as LaTeX files for my Bachelor Thesis and presentation.

The formalization makes use of the Coq-stdpp library.
Comments in the Coq files refer to the numbering of lemmas in the paper [A New Approach for Active Automata Learning Based on Apartness](https://rdcu.be/cUzCo).
