\documentclass{beamer}

\usepackage[british]{babel}
\usepackage{graphicx,hyperref,ru,url}
\usepackage{amsmath,amssymb,amsthm}
\usepackage[inline]{enumitem}
\usepackage{stmaryrd}
\usepackage{array}
\usepackage{listings}
\usepackage{minted}
\usepackage{xspace}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{bold-extra}
\usepackage{fontspec}
\setmonofont{DejaVu Sans Mono}
\usepackage{contour}
\usepackage{tikz}
\usetikzlibrary{automata, arrows.meta, positioning, patterns, patterns.meta, intersections,
shapes.geometric, calc, arrows, decorations.pathreplacing,decorations.pathmorphing}



\newcommand{\sem}[1]{\llbracket #1 \rrbracket}
\newcommand{\pto}{\rightharpoonup}
\newcommand{\empwor}{\epsilon}
\newcommand{\defined}{\!\!\downarrow}
\newcommand{\undefined}{\!\!\uparrow}
\newcommand{\transition}[4]{#1\xrightarrow{#2/#3}#4}
\newcommand{\stdpp}{{stdpp}\xspace}
\newcommand{\access}{\textsf{access}}
\newcommand{\id}{\textsf{id}}
\newcommand{\outquery}{\textsc{OutputQuery}}
\newcommand{\equivquery}{\textsc{EquivQuery}}
\newcommand{\apart}{\mathbin{\#}}
\newcommand{\letcon}[2]{#1\mathbin{:}#2}
\newcommand{\con}[2]{#1\ #2}

\renewcommand{\Lsh}{$L^\#$\xspace}
\newcommand{\Lst}{$L^*$\xspace}
\newcommand{\query}[3]{\textsc{\textbf{#1}}(#2)\textbf{\textsc{:}} \textit{#3}}
\newcommand{\surl}[1]{\begin{scriptsize}\url{#1}\end{scriptsize}}

\contourlength{0.1em}
\contournumber{64}
\newcommand{\cont}[1]{\contour{white}{#1}}

\newcommand{\m}[1]{\mintinline[fontsize=\scriptsize]{coq}{#1}}
\newminted{coq}{fontsize=\scriptsize, autogobble}
\newmintedfile{coq}{fontsize=\scriptsize, breaklines=true}
\newcommand{\ans}[1]{\begin{small}\texttt{#1}\end{small}}

% The title of the presentation:
%  - first a short version which is visible at the bottom of each slide;
%  - second the full title shown on the title slide;
\title[\Lsh in Coq]{
  Towards a Formalization of \Lsh in Coq}

% Optional: a subtitle to be dispalyed on the title slide
%\subtitle{Show where you're from}

% The author(s) of the presentation:
%  - again first a short version to be displayed at the bottom;
%  - next the full list of authors, which may include contact information;
\author[Sander Suverkropp]{
  Sander Suverkropp \\\medskip
  { \small
  Supervisor:  Dr. Freek Wiedijk\\
  Second assessor:  Dr. Jurriaan Rot
  }}

% The institute:
%  - to start the name of the university as displayed on the top of each slide
%    this can be adjusted such that you can also create a Dutch version
%  - next the institute information as displayed on the title slide
\institute[Radboud University Nijmegen]{
  Computer Science\\
  Radboud University Nijmegen}

% Add a date and possibly the name of the event to the slides
%  - again first a short version to be shown at the bottom of each slide
%  - second the full date and event name for the title slide
\date[August 26 2022]{
  August 26 2022}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Outline:
% Active automata learning
% MAT
% Mealy machines
% MAT for Mealy
% L# in general
% Coq:Mealy
% Hypothesis creation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Active Automata Learning}

\begin{frame}
  \frametitle{Active Automata Learning}

  \begin{itemize}
    \item Learning a regular language/DFA
    \item Game between teacher and learner
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Active Automata Learning}

  \begin{itemize}
    \item Learning language $L$
  \end{itemize}

  \begin{block}{Membership query}
    Is word $w$ in $L$?
  \end{block}
  \begin{block}{Equivalence query}
    Is the language accepted by DFA $H$ the same as $L$?\\
    If it isn't: counterexample in $L(H)\setminus L \cup L \setminus L(H)$
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Mealy machines}

  \begin{center}
    \begin{tikzpicture}[node distance = 3cm, on grid, auto]
      \node (q0) [state, initial, initial text = {}] {$q_0$};
      \node (q1) [state, right=of q0] {$q_1$};
      \node (q2) [state, right=of q1] {$q_2$};
      \node (q3) [state, below=of q1] {$q_3$};
      \path [-stealth, thick]
      (q0) edge node[above]               {a/B} (q1)
      (q0) edge node                      {b/B} (q3)
      (q1) edge node [below]              {a/A} (q2)
      (q1) edge [loop above]  node        {b/B} ()
      (q2) edge node                      {b/B} (q3)
      (q3) edge node [right] {\begin{tabular}{c} a/B \\ b/A \end{tabular}}
                                                (q1)
      (q2) edge [bend right] node [above] {a/B} (q1)
      ;
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Active Automata Learning for Mealy Machines}

Learning Mealy machine $M$

  \begin{block}{Output query}
    What is the output of $M$ given input word $w$?
  \end{block}
  \begin{block}{Equivalence query}
    Is the behaviour of Mealy machine $H$ the same as $M$?\\
    If it isn't: counterexample with different output
  \end{block}
\end{frame}

\section{The \Lsh algorithm}

\begin{frame}[fragile]
  \frametitle{The \Lsh algorithm}

  \begin{itemize}
    \item Apartness
    \item Partial Mealy machine: \emph{Observation tree}
    \item Basis and frontier
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{The \Lsh algorithm}

  \begin{figure}[h!]
    \centering
    \begin{tikzpicture}[node distance=2cm, on grid, auto, scale=0.8,
      basis/.style={pattern=vertical lines,   pattern color=red,   distance=20pt},
      frontier/.style={pattern=north west lines, pattern color=blue,  distance=20pt}]
      \node (t0) [state, basis, initial, initial text = {}] at (0,1) {\cont{$t_0$}};
      \node (t1) [state, frontier] at (2,2) {\cont{$t_1$}};
      \node (t2) [state, basis] at (2,0) {\cont{$t_2$}};
      \node (t3) [state, basis] at (4,0) {\cont{$t_3$}};
      \node (t4) [state, frontier] at (6,0) {\cont{$t_4$}};
      \node (t5) [state, frontier] at (4,2) {\cont{$t_5$}};
      \node (t6) [state, frontier] at (6,2) {\cont{$t_6$}};
      \node (t1a) [state] at (4,4) {\cont{$t_7$}};
      \node (t4a) [state] at (8,0) {\cont{$t_8$}};
      \node (t5a) [state] at (6,4) {\cont{$t_9$}};
      \node (t6a) [state] at (8,2) {\cont{$t_{10}$}};
      \path [-stealth, thick]
      (t0) edge node             {a/A} (t1)
      (t0) edge node[below left] {b/B} (t2)
      (t2) edge node[below]      {b/B} (t3)
      (t3) edge node[below]      {a/C} (t4)
      (t2) edge node[left]       {a/A} (t5)
      (t3) edge node[left]       {b/B} (t6)
      (t1) edge node[left]       {a/A} (t1a)
      (t4) edge node             {a/A} (t4a)
      (t5) edge node[left]       {a/A} (t5a)
      (t6) edge node             {a/C} (t6a)
      ;
    \end{tikzpicture}
  \end{figure}
\end{frame}

\section{Formalization}

\begin{frame}
  \frametitle{Coq}

  \begin{itemize}
    \item Proof assistent
    \item CoC
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Mealy machines in Coq}

  \begin{block}{Mealy machines in Coq}
    \begin{coqcode}
Record mealy (input output : Type) `{Countable input} :=
  Mealy {
    Q :> Set;
    eqDecisionQ : EqDecision Q;
    finiteQ : Finite Q;
    q0 : Q;
    transition : gmap (Q * input) (Q * output)
  }.
    \end{coqcode}
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Questions?}

  %TODO fill slide
\end{frame}

% =============================================================
%                          Extra slides
% =============================================================

\begin{frame}
  \frametitle{Extra info to answer questions}

\end{frame}

\end{document}
